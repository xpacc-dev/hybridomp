#include <stdio.h>
#include <chrono>
#include <iostream>
#include <openacc.h>

using namespace std;

extern "C" void zaxpy_(long *start, long *end, long *len, double *X, double *Y, double *Z);


int main(int argc, char const* argv[])
{
  double *X, *Y, *Z;
  long len = atoi(argv[1]);

  X = (double *) malloc(len * sizeof(double));
  Y = (double *) malloc(len * sizeof(double));
  Z = (double *) malloc(len * sizeof(double));

  for (int i=0; i<len; i++) {
    X[i] = 1.5;
    Y[i] = 2.3;
    Z[i] = 0.0;
  }
  long len2 = len/2;
  long zero = 0;

  // Only run offloaded part for OpenAcc
  #pragma acc data copy(X, Y, Z, zero, len, len2)
  {
    chrono::steady_clock::time_point begin = chrono::steady_clock::now();
    #pragma acc kernels
      zaxpy_(&zero, &len2, &len, X, Y, Z);
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    cout << "kernel time GPU (microseconds) = " << chrono::duration_cast<chrono::microseconds>(end - begin).count() <<endl;
  }
  
  printf("#dev: %d len: %ld %.1f %.1f\n", acc_get_num_devices(acc_device_nvidia), len, Z[len2-4], Z[len2+4]);

  return 0;
}
