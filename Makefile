F90=./gcc-offload/install/bin/gfortran
CXX=./gcc-offload/install/bin/g++
CC=./gcc-offload/install/bin/gcc

CXXFLAGS=-Wall -O2 -g -fno-exceptions
FFLAGS=-Wall -O2 -g

all: testomp

testomp: kernelomp.f90 testomp.cpp Makefile
	$(F90) $(FFLAGS) -fopenmp -c kernelomp.f90
	$(CXX) $(CXXFLAGS) -fopenmp testomp.cpp kernelomp.o -o $@
	LD_LIBRARY_PATH=./gcc-offload/install/lib64/ ./$@ 1024000

testacc: kernelacc.f90 testacc.cpp Makefile
	gfortran $(FFLAGS)   -fopenacc -c kernelacc.f90
	g++      $(CXXFLAGS) -fopenacc testacc.cpp kernelacc.o -o $@
	./$@ 1024000

testcuda: testcuda.cu Makefile
	/usr/local/cuda-8.0/bin/nvcc -O2 -Wno-deprecated-gpu-targets $<  -lcublas -std=c++11 -lcurand -g -o $@
	./$@ 1024000


clean:
	rm -f testomp testacc testcuda *.o
