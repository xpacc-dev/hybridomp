#include <stdio.h>
#include <omp.h>
#include <chrono>
#include <iostream>

using namespace std;

int num_gpus = -1;

extern "C" void zaxpy_(long *start, long *end, long *len, double *X, double *Y, double *Z);

void gpu_available(void)
{
  if (num_gpus > -1)
    return;
  int A[1] = {-1};

  #pragma omp target
  {
    A[0] = omp_is_initial_device();
  }


  // measure time to start an (empty) kernel
  chrono::steady_clock::time_point begin = chrono::steady_clock::now();
  #pragma omp target
  {

  }

  chrono::steady_clock::time_point end= chrono::steady_clock::now();
  cout << "GPU kernel startup time (microseconds) = " << chrono::duration_cast<chrono::microseconds>(end - begin).count() <<endl;

  printf("able to use offloading: %s, #dev %d\n", A[0]==0?"yes":"NO", omp_get_num_devices());
  num_gpus = A[0]==0 ? omp_get_num_devices() : 0;
}


int main(int argc, char const* argv[])
{
  double *X, *Y, *Z;
  long len = atoi(argv[1]);
  int i;

  X = (double *) malloc(len * sizeof(double));
  Y = (double *) malloc(len * sizeof(double));
  Z = (double *) malloc(len * sizeof(double));

  gpu_available();

  for (i=0; i<len; i++) {
    X[i] = 1.5;
    Y[i] = 2.3;
    Z[i] = 0.0;
  }
  long len2 = len/1.1;

#pragma omp parallel num_threads(5)
#pragma omp target data map(tofrom:Z[:len],Y[:len],X[:len])
  {
    int tid = omp_get_thread_num();
    long zero = 0;
    chrono::steady_clock::time_point begin2 = chrono::steady_clock::now();
    
    if (tid == 4) {
      {
        chrono::steady_clock::time_point begin = chrono::steady_clock::now();
        #pragma omp target teams
          zaxpy_(&zero, &len2, &len, X, Y, Z);
        chrono::steady_clock::time_point end = chrono::steady_clock::now();
        cout << "kernel time GPU (microseconds) = " << chrono::duration_cast<chrono::microseconds>(end - begin).count() <<endl;
      }
    } else {
      chrono::steady_clock::time_point begin = chrono::steady_clock::now();
      zaxpy_(&len2, &len, &len, X, Y, Z);
      chrono::steady_clock::time_point end = chrono::steady_clock::now();
      cout << "kernel time CPU (microseconds) = " << chrono::duration_cast<chrono::microseconds>(end - begin).count() <<endl;
    }
  #pragma omp barrier
  chrono::steady_clock::time_point end2 = chrono::steady_clock::now();
  #pragma omp master
  cout << "full time (milliseconds) = " << chrono::duration_cast<chrono::milliseconds>(end2 - begin2).count() <<endl;
  }
  printf("len: %ld %.1f %.1f\n", len, Z[4], Z[len-4]);

  return 0;
}
